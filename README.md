# example



    cat << EOF | kubectl apply -f -

    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: tiller
      namespace: kube-system

    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: tiller
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: cluster-admin
    subjects:
      - kind: ServiceAccount
        name: tiller
        namespace: kube-system
    EOF
    
    helm init --service-account tiller


## spa

    git clone https://github.com/egeneralov/spa-example.git
    cd spa-example

    # download patch
    wget https://gitlab.com/18.04.2019/tpl/raw/master/spa-deployment.yaml.patch
    # download .gitlab-ci.yml
    wget https://gitlab.com/18.04.2019/tpl/raw/master/.gitlab-ci.yml

    # create chart
    helm create spa
    
    # apply changes
    patch -t spa/templates/deployment.yaml < spa-deployment.yaml.patch

    # move to place
    mkdir -p .helm
    mv spa .helm/


## terraform-backend-http


    git clone https://github.com/egeneralov/terraform-state-backend.git
    cd terraform-state-backend

    # download patch
    wget https://gitlab.com/18.04.2019/tpl/raw/master/terraform-values.yaml.patch
    wget https://gitlab.com/18.04.2019/tpl/raw/master/terraform-deployment.yaml.patch
    # download .gitlab-ci.yml
    wget https://gitlab.com/18.04.2019/tpl/raw/master/.gitlab-ci.yml

    # create chart
    helm create terraform-backend-http
    
    # download postgres chart
    helm fetch stable/postgresql
    tar xzvf postgresql-*.tgz -C terraform-backend-http/charts/
    rm postgresql-*.tgz
    
    # apply changes
    patch -t terraform-backend-http/values.yaml < terraform-values.yaml.patch
    patch -t terraform-backend-http/templates/deployment.yaml < terraform-deployment.yaml.patch
    
    # move to place
    mkdir -p .helm
    mv terraform-backend-http .helm/



